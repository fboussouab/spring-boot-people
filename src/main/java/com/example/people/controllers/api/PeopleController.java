package com.example.people.controllers.api;

import com.example.people.domain.Person;
import com.example.people.services.PeopleService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "api/people", produces = APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class PeopleController {
    private final PeopleService peopleService;
    @GetMapping
    public ResponseEntity<Iterable<Person>> index() {
        return ResponseEntity.ok(peopleService.readAll());
    }

    @GetMapping(path = "{personId}")
    public ResponseEntity<Person> readPerson(@PathVariable int personId){
        Person person = peopleService.getPerson(personId);
        if (person == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(person);
    }

    @PostMapping(path = "add", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> addPerson(@Valid @RequestBody Person person){
        Person createdPerson = peopleService.addPerson(person);
        return new ResponseEntity<>(createdPerson, CREATED);
    }

    @PutMapping(path = "edit/{personId}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> editPerson(@Valid @RequestBody Person person, @PathVariable int personId){
        Person foundPerson = peopleService.getPerson(personId);
        if (foundPerson == null) {
            return ResponseEntity.notFound().build();
        }
        foundPerson.setFirstName(person.getFirstName());
        foundPerson.setLastName(person.getLastName());
        foundPerson.setEmail(person.getEmail());
        peopleService.addPerson(foundPerson);
        return ResponseEntity.ok(foundPerson);
    }

    @DeleteMapping(path = "delete/{personId}")
    public ResponseEntity<Person> removePerson(@PathVariable int personId){
        Person person = peopleService.getPerson(personId);
        if (person == null) {
            return ResponseEntity.notFound().build();
        }
        peopleService.removePerson(person);
        return ResponseEntity.ok(person);
    }
}
