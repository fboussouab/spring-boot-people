package com.example.people.services;

import com.example.people.domain.Person;
import com.example.people.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PeopleService {
    private final PeopleRepository peopleRepository;

    public Person getPerson(int personId) {
        return peopleRepository.findById(personId).orElse(null);
    }

    public Person addPerson(Person person) {
        return peopleRepository.saveAndFlush(person);
    }

    public List<Person> readAll() {
        return peopleRepository.findAll();
    }

    public void removePerson(Person person) {
        peopleRepository.delete(person);
    }
}
