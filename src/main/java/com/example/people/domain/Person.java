package com.example.people.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "PEOPLE")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PERSON_ID")
    private int personId;

    @Column(name = "FIRST_NAME")
    @NotBlank(message = "firstName is mandatory")
    private String firstName;

    @Column(name = "LAST_NAME")
    @NotBlank(message = "lastName is mandatory")
    private String lastName;

    @Column(name = "EMAIL")
    private String email;

}
